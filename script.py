import os
from itertools import product

b = [100.0, 16.0]
c = [0.05, 0.1, 0.25, 0.5, 0.75, 1.0]

for (i,j) in product(b,c):
    print('Running: python incomp-FF.py '+str(i)+' '+str(j)+' '+str(0))
    os.system('python incomp-FF.py '+str(i)+' '+str(j)+' '+str(0))
    print('--------')
