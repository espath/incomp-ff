"""
1D couple equations
This script should be ran serially (because it is 1D), and creates a space-time
plot of the computed solution.

Espath LFR

python incomp-FF.py 100.0 0.1 0
"""

import numpy as np
from scipy.optimize import fsolve
from functools import partial
import time, sys
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import cm

from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)
matplotlib.rcParams['xtick.labelsize'] = 14
matplotlib.rcParams['ytick.labelsize'] = 14
style = {
        'font.size': 14,
        'pgf.texsystem': 'pdflatex',
        'text.usetex': True,
        'pgf.preamble': [
         r'\usepackage[utf8x]{inputenc}',
         r'\usepackage[T1]{fontenc}',
         r'\usepackage{cmbright}',
         ]
        }
matplotlib.rcParams.update(style)
#matplotlib.rcParams.update({'font.size': 14})

from dedalus import public as de
from dedalus.extras.plot_tools import quad_mesh, pad_limits

import logging
logger = logging.getLogger(__name__)

plot_show = int(sys.argv[3])

# Parameters
b = float(sys.argv[1])#100.0
c = float(sys.argv[2])#0.1
a = (b**0.5*np.sinh(b**0.5))/(np.cosh(b**0.5)-1.0)
Lx = 2.
nx = 1024
n_terms = 1000

# Bases and domain
x_basis = de.Chebyshev('x',nx, interval=(-Lx/2, Lx/2), dealias=3/2)
domain = de.Domain([x_basis], grid_dtype=np.float64)

# Problem
problem = de.IVP(domain, variables=['f', 'fx', 'h', 'hx'])
problem.parameters['a']  = a
problem.parameters['b']  = b
problem.parameters['c']  = c
problem.add_equation("dt(f) - dx(fx) - hx = a")
problem.add_equation("fx - dx(f) = 0")
problem.add_equation("dt(h) - c*dx(hx) - c*b*fx = 0")
problem.add_equation("hx - dx(h) = 0")

problem.add_bc("left(f) = 0")
problem.add_bc("right(f) = 0")
problem.add_bc("left(h) = 0")
problem.add_bc("right(h) = 0")

# Build solver
solver = problem.build_solver(de.timesteppers.SBDF2)
solver.stop_wall_time = 60
solver.stop_iteration = 45000

# Initial conditions
x = domain.grid(0)
f = solver.state['f']
fx = solver.state['fx']
h = solver.state['h']
hx = solver.state['hx']

def s_eq(n_terms,sast):
    sum_aux = 0.0
    for i in range(1,n_terms):
        sum_aux += 8.0/((2.0*i-1.0)**2*np.pi**2)*np.exp(-(2.0*i-1.0)**2*np.pi**2*sast/4.0)
    return sum_aux-1.0+2.0*(np.cosh(b**0.5)-1.0)/(b**0.5*np.sinh(b**0.5))

equation = partial(s_eq,n_terms)

sast = fsolve(equation, 0.1)

# f['g'] = (1.0-x**2)
def aux(n_terms,x,sast):
    sum_aux = 0.0
    for i in range(1,n_terms):
        sum_aux += 16.0/((2.0*i-1.0)**3*np.pi**3)*np.sin((2.0*i-1.0)*np.pi*(1.0+x)/2.0)*np.exp(-(2.0*i-1.0)**2*np.pi**2*sast/4.0)
    return sum_aux

# factor = 2.0*(np.cosh(b**0.5)-1.0)/(b**0.5*np.sinh(b**0.5))
# sast = 4.0*np.log(8.0/(np.pi**2*(1-factor)))/np.pi**2
f['g'] = b**0.5*np.sinh(b**0.5)/(np.cosh(b**0.5)-1.0)*((1.0-x**2)/2.0-aux(n_terms,x,sast))
f.differentiate(0, out=fx)
h['g'] = 0.
h.differentiate(0, out=hx)

# Store data for final plot
f.set_scales(1)
h.set_scales(1)
fx.set_scales(1)
hx.set_scales(1)
f_list = [np.copy(f['g'])]
h_list = [np.copy(h['g'])]
fx_list = [np.copy(fx['g'])]
hx_list = [np.copy(hx['g'])]
t_list = [solver.sim_time]

# Main loop
dt = 1.e-4
try:
    logger.info('Starting loop')
    start_time = time.time()
    while solver.proceed:
        solver.step(dt)
        if solver.iteration % 20 == 0:
            f.set_scales(1)
            f_list.append(np.copy(f['g']))
            fx_list.append(np.copy(fx['g']))
            h.set_scales(1)
            h_list.append(np.copy(h['g']))
            hx_list.append(np.copy(hx['g']))
            t_list.append(solver.sim_time)
        if solver.iteration % 100 == 0:
            logger.info('Iteration: %i, Time: %e, dt: %e' %(solver.iteration, solver.sim_time, dt))
except:
    logger.error('Exception raised, triggering end of main loop.')
    raise
finally:
    end_time = time.time()
    logger.info('Iterations: %i' %solver.iteration)
    logger.info('Sim end time: %f' %solver.sim_time)
    logger.info('Run time: %.2f sec' %(end_time-start_time))
    logger.info('Run time: %f cpu-hr' %((end_time-start_time)/60/60*domain.dist.comm_cart.size))

# Create space-time plot
#############
ww, hh = plt.figaspect(1.0/2.5)
fig = plt.figure(figsize=(ww, hh))
f_array = np.array(f_list)
fx_array = np.array(fx_list)
t_array = np.array(t_list)
xmesh, ymesh = quad_mesh(x=t_array, y=x)
plt.pcolormesh(xmesh, ymesh, f_array.T, cmap='RdBu_r')
plt.axis(pad_limits(xmesh, ymesh))
cb = plt.colorbar()
for t in cb.ax.get_yticklabels():
    t.set_horizontalalignment('right')
    t.set_x(3.5)
X, Y = np.meshgrid(t_array, x)
#--
ff_array = f_array[::4,::4].T
XX, YY = np.meshgrid(t_array[::4], x[::4])
a_p = np.stack([XX.ravel(),YY.ravel(),ff_array.ravel()]).T
np.savetxt('solution_f_'+str(b)+'_'+str(c)+'_.txt', a_p, fmt='%1.3e')
ffx_array = np.abs(fx_array[::4,::4])**2#/np.max(np.abs(fx_array[::4,::4].T)**2)
# for ii in range(ffx_array.shape[0]): ffx_array[ii,:] = ffx_array[ii,:]/np.max(ffx_array[ii,:])
ffx_array = ffx_array.T
a_p = np.stack([XX.ravel(),YY.ravel(),ffx_array.ravel()]).T
np.savetxt('solution_fx_'+str(b)+'_'+str(c)+'_.txt', a_p, fmt='%1.3e')
#--
C = plt.contour(X, Y, f_array.T, 6, linewidths = 1, colors='black')
plt.clabel(C, inline=1, fontsize=10, fmt=r'%1.2f')
plt.xlabel(r'$\bar{t}$',labelpad=10,linespacing=5.0,rotation='horizontal')
plt.ylabel(r'$\bar{x}_2$',labelpad=12,linespacing=5.0,rotation='horizontal')
fig.tight_layout()
plt.savefig('f.png')
#############
fig = plt.figure(figsize=plt.figaspect(0.5))
ax = fig.gca(projection='3d')
X, Y = np.meshgrid(x, t_array)
surf = ax.plot_surface(X, Y, f_array, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)
ax.set_xlabel(r'$\bar{x}_2$')
ax.set_ylabel(r'$\bar{t}$')
ax.set_zlabel(r'$\bar{v}$')
ax.xaxis.labelpad=8
ax.yaxis.labelpad=8
ax.zaxis.labelpad=5
fig.tight_layout()
plt.savefig('surf_f.png')
#############
ww, hh = plt.figaspect(1.0/2.5)
plt.figure(figsize=(ww, hh))
h_array = np.array(h_list)
hx_array = np.array(hx_list)
t_array = np.array(t_list)
xmesh, ymesh = quad_mesh(x=t_array, y=x)
plt.pcolormesh(xmesh, ymesh, h_array.T, cmap='RdBu_r')
plt.axis(pad_limits(xmesh, ymesh))
cb = plt.colorbar()
for t in cb.ax.get_yticklabels():
    t.set_horizontalalignment('right')
    t.set_x(3.5)
X, Y = np.meshgrid(t_array, x)
#--
hh_array = h_array[::4,::4].T
XX, YY = np.meshgrid(t_array[::4], x[::4])
a_p = np.stack([XX.ravel(),YY.ravel(),hh_array.ravel()]).T
np.savetxt('solution_g_'+str(b)+'_'+str(c)+'_.txt', a_p, fmt='%1.3e')
hhx_array = np.abs(hx_array[::4,::4])**2/b#/np.max(np.abs(hx_array[::4,::4].T)**2)
# for ii in range(hhx_array.shape[0]): hhx_array[ii,:] = hhx_array[ii,:]/(np.max(hhx_array[ii,:]) or not np.max(hhx_array[ii,:]))
hhx_array = hhx_array.T
a_p = np.stack([XX.ravel(),YY.ravel(),hhx_array.ravel()]).T
np.savetxt('solution_gx_'+str(b)+'_'+str(c)+'_.txt', a_p, fmt='%1.3e')
#--
C = plt.contour(X, Y, h_array.T, 6, linewidths = 1, colors='black')
plt.clabel(C, inline=1, fontsize=10, fmt=r'%1.2f')
plt.xlabel(r'$\bar{t}$',labelpad=10,linespacing=5.0,rotation='horizontal')
plt.ylabel(r'$\bar{x}_2$',labelpad=12,linespacing=5.0,rotation='horizontal')
fig.tight_layout()
plt.savefig('g.png')
#############
fig = plt.figure(figsize=plt.figaspect(0.5))
ax = fig.gca(projection='3d')
X, Y = np.meshgrid(x, t_array)
surf = ax.plot_surface(X, Y, h_array, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)
ax.set_xlabel(r'$\bar{x}_2$')
ax.set_ylabel(r'$\bar{t}$')
ax.set_zlabel(r'$\bar{G}$')
ax.xaxis.labelpad=8
ax.yaxis.labelpad=8
ax.zaxis.labelpad=5
fig.tight_layout()
plt.savefig('surf_g.png')

def f_fun(x,b):
    return 1.0-(1.0-np.cosh(x*b**0.5))/(1.0-np.cosh(b**0.5))

def g_fun(x,b):
    return b**0.5*np.sinh(b**0.5)/(np.cosh(b**0.5)-1.0)*(np.sinh(x*b**0.5)/np.sinh(b**0.5)-x)

fig = plt.figure()
plt.plot(x,f_array[0],'b-',label='initial')
plt.plot(x,f_array[-1],'k-',label='final')
plt.plot(x,f_fun(x,b),'r:',linewidth=5.0,label='asymptotic')
plt.ylabel(r'$\bar{v}$',rotation='horizontal')
plt.xlabel(r'$\bar{x}_2$')
fig.tight_layout()
plt.savefig('ini_fin_f.pdf')

fig = plt.figure()
plt.plot(x,h_array[0],'b-',label='initial')
plt.plot(x,h_array[-1],'k-',label='final')
plt.plot(x,g_fun(x,b),'r:',linewidth=5.0,label='asymptotic')
plt.ylabel(r'$\bar{G}$',rotation='horizontal')
plt.xlabel(r'$\bar{x}_2$')
fig.tight_layout()
plt.savefig('ini_fin_g.pdf')

fig = plt.figure()
plt.plot(t_array,f_array[:,np.int(nx/2)],'b-',label='time profile')
plt.title(r'$\bar{v}(0,t)$')
plt.ylabel(r'$\bar{v}$',rotation='horizontal')
plt.xlabel(r'$\bar{t}$')
fig.tight_layout()
plt.savefig('time_profile_f.pdf')
# a_p = np.stack([t_array,f_array[:,np.int(nx/2)]]).T
# np.savetxt('profile_f.txt', a_p, fmt='%1.6e')

if (plot_show==1): plt.show()
